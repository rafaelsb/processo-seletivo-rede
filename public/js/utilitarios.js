$(function () {

    ler();

    //previne que o metodo editar seja chamado quando o usuario quiser salvar um novo registro
    $("#limpar").click(function(){
        $("#formSalvar").attr('action','../public/salvar');
    });
});

function ler(){
    $.ajax({
        type: "get",
        url: '../public/ler',
        success: function (data) {
            popularTable(data);
        },
        error: function (data) {
            swal("Erro", "Não Foi Possível buscar os usuarios registrados", "error");
        }
    });
}


function edicao(id) {
    $.ajax({
        type: "get",
        url: '../public/get/'+id,
        success: function (data) {
            $("#idEdit").val(data.id);
            $("#nome").val(data.nome);
            $("#email").val(data.email);
            $("#tel").val(data.telefone);
            $("#formSalvar").attr('action','../public/editar');
        },
        error: function (data) {
            swal("Erro", "Não Foi Possível buscar o usuário selecionado", "error");
        }
    });
}

function apagar(id) {
    $.confirm({
        title: 'Confirma a Exclusão?',
        content: 'Ação Irreversível!',
        buttons: {
            sim: {
                action: function () {
                    $("#deleteId").val(id);
                    $("#formDelete").submit();
                },
                btnClass: 'btn btn-danger'
            },
            "Não": {}
        }
    });
}


function popularTable(data) {
    dadosTable = [];
    $.each(data, function (key, val) {
        //adicionando valores para datatable
        console.log(val);
        var nome = val.nome;
        var email = val.email;
        var telefone = val.telefone;
        var btns = '<button class="btn btn-danger"  onclick="apagar(' + val.id + ')"><i class="fa fa-trash" ></i></button> ' +
            ' <button class="btn btn-info"  onclick="edicao(' + val.id + ')"><i class="fa fa-pencil" ></i></button>';
        arr = [nome, email, telefone, btns];
        dadosTable.push(arr);
    });

    var data = new Date();
    dia = data.getDate() + "/" + (data.getMonth() + 1) + "/" + data.getFullYear();
    $("#listaUsuarios").DataTable({
        language: languageTable(),
        "destroy": true,
        "autoWidth": false,
        dom: 'Bfrtip',
        buttons: [{
            extend: 'excelHtml5',
            messageTop: "Dados extraidos no dia " + dia,
            title: "Relatório de Usuários",
            footer: true
        }
        ],
        responsive: true,
        data: dadosTable,
        columns: [
            {title: "Nome"},
            {title: "E-mail"},
            {title: "Telefone"},
            {title: "<i class='fa fa-cogs'></i>"}
        ]
    });
}


function languageTable() {
    return {
        "sEmptyTable": "Nenhum registo encontrado",
        "sInfo": "Mostrando de _START_ at&eacute; _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 at&eacute; 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ registros  por p&aacute;gina",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Pr&oacute;ximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "�ltimo"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    }
}

