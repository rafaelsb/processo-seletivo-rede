<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'home','uses'=> function () {
    return view('inicio');
}]);

Route::get('/ler/',function(){
    $jsonString = file_get_contents(asset('js/usuarios.json'));
    $data = json_decode($jsonString, true);
   return response()->json($data);
});

Route::post('/salvar',['as'=>'salvar','uses'=>'UsuarioController@salvar']);
Route::post('/apagar',['as'=>'apagar','uses'=>'UsuarioController@apagar']);
Route::get('/get/{id}',['as'=>'get','uses'=>'UsuarioController@getById']);
Route::post('/editar',['as'=>'editar','uses'=>'UsuarioController@editar']);
