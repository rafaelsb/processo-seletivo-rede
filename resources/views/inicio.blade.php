@extends('layouts.site')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper login-box">
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content login-box-body">
            @include('layouts._messages')
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Cadastro de Usuários</a></li>
                </ol>
            </nav>
            <!-- Small boxes (Stat box) -->
            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-md-12 conteudo">
                    <div class="box box-primary">
                        <div class="box-body">
                            <form method="post" action="{{route('salvar')}}" id="formSalvar">
                                {{ csrf_field() }}
                                <input type="hidden" name="idDespesa" id="idDespesa" value="sn">
                                <div class="input-group col-md-12 ">
                                    <input type="hidden" name="idEdit" id="idEdit"/>
                                    <div class="col-md-4 col-xs-6">
                                        <span>Nome</span>
                                        <input type="text" name="nome" id="nome" class="form-control" required placeholder="nome e sobrenome"/>
                                    </div>
                                    <div class="col-md-4 col-xs-6">
                                        <span>Email</span>
                                        <input type="email" name="email" id="email" class="form-control" required placeholder="exemplo@exemplo.com"/>
                                    </div>
                                    <div class="col-md-2 col-xs-6">
                                        <span>Telefone</span>
                                        <input type="text" name="tel" id="tel" class="form-control tel" required placeholder="ex: (71) 99999-9999">
                                    </div>
                                    <div class="col-md-1  col-xs-12">
                                        <br/>
                                        <input type="submit" value="Salvar" class="btn btn-info form-control"
                                        />
                                    </div>
                                    <div class="col-md-1  col-xs-12">
                                        <br/>
                                        <input type="reset" value="Limpar" id="limpar"
                                               class="btn btn-default  form-control"
                                        />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Usuários Registrados</h3>
                                </div>

                                <div class="box-body tableScroll">
                                    <table id="listaUsuarios" class="table table-hover table-striped">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--forms edit and delete-->
            <form id="formDelete" method="post" action="{{route('apagar')}}">
                {{ csrf_field() }}
                <input type="hidden" name="deleteId" id="deleteId">
            </form>
        </section>
    </div>
@endsection
