@if(Session::has('mensagem'))
    <div class="alert {{Session::get('mensagem')['class']}} alert-dismissible fade show" role="alert">
        <strong>Aviso!</strong> {{Session::get('mensagem')['msg']}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if(Session::has('mensagemPop'))
    <input type="hidden" value="{{Session::get('mensagemPop')['msg']}}" id="popMsg"/>
    <input type="hidden" value="{{Session::get('mensagemPop')['class']}}" id="popClass"/>
    <script>
        swal("Mensagem", document.getElementById('popMsg').value, document.getElementById('popClass').value);
    </script>
@endif
@if(!empty($errors->all()))
    @foreach($errors->all() as $error)
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Erro!</strong> {{$error}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endforeach
@endif
