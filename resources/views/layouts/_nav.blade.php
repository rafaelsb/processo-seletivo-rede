<!-- Logo -->
<a class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">PS</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">Processo Seletivo</span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
        <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
           <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a class="dropdown-toggle" target="_blank" href="https://www.linkedin.com/in/rafael-santana-de-brito-b2770536/" >
                    <span class="hidden-xs">Rafael Santana de Brito <i class="fa fa-linkedin"></i></span>
                </a>
            </li>
        </ul>
    </div>
</nav>
