<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Rede Industrial</title>

    <link id="favicon" rel="icon" type="image/png" sizes="64x64" href="{{asset('img/icon.png')}}">

    <!-- Styles -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/skins/_all-skins.min.css')}}">
    <link rel="stylesheet" href="{{asset('https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css')}}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('libs/font-awesome/css/font-awesome.min.css')}}">

    <!-- Tema style -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <!-- Scripts -->
    <!-- jQuery 3 -->
    <script src="{{asset('libs/jquery/dist/jquery.min.js')}}"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    <!-- datatable -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <!--BTNs table-->
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

    {{-- confirmation--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

    <!-- scripts-->
    <script src="{{asset('js/utilitarios.js')}}"></script>


    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>

    <!-- Bootstrap -->
    <script src="{{asset('libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('libs/sweetalert.min.js') }}"></script>

    <!-- AdminLTE App -->
    <script src="{{asset('js/adminlte.js')}}"></script>

    <script src="{{asset('libs/jquery/jquery.mask.min.js')}}"></script>
    <script>

        $(function () {
            $(".tel").mask('(99) 99999-9999');
        });

    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<header class="main-header">
    @include('layouts._nav')
</header>
<main>
    @yield('content')
</main>
@include('layouts._footer')
</body>
</html>
