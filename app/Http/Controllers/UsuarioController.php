<?php


namespace App\Http\Controllers;

setlocale(LC_ALL, 'pt_BR.UTF8');
mb_internal_encoding('UTF8');
mb_regex_encoding('UTF8');

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Usuario;

class UsuarioController extends Controller
{

    function salvar(Request $request)
    {
        $this->validate($request, Usuario::$rules, Usuario::$mensagens);

        if (Usuario::verificaEmailAndTel($request->email, $request->tel)) {
            \Session::flash('mensagem', ['msg' => 'Este email ou telefone já existe em nossa base ', 'class' => 'alert-warning']);
        } else {
            // Read File
            if (file_put_contents(base_path('public/js/usuarios.json'), stripslashes(Usuario::salvar($request)))) {
                \Session::flash('mensagemPop', ['msg' => 'Novo Usuário Registrado', 'class' => 'success']);
            } else {
                \Session::flash('mensagemPop', ['msg' => 'Houve um problema ao cadastar o usuario, por favor tente novamente', 'class' => 'error']);
            }
        }
        return redirect()->route('home')->withInput();

    }

    function apagar(Request $request)
    {
        if (file_put_contents(base_path('public/js/usuarios.json'), stripslashes(Usuario::apagar($request)))) {
            \Session::flash('mensagemPop', ['msg' => 'Usuário Removido', 'class' => 'success']);
        } else {
            \Session::flash('mensagemPop', ['msg' => 'Houve um problema ao remover o usuario, por favor tente novamente', 'class' => 'error']);
        }
        return redirect()->route('home');
    }

    function editar(Request $request)
    {
        $this->validate($request, Usuario::$rules, Usuario::$mensagens);

        if (Usuario::verificaEmailAndTel($request->email, $request->tel,$request->idEdit)) {
            \Session::flash('mensagem', ['msg' => 'Este email ou telefone já existe em nossa base ', 'class' => 'alert-warning']);
        } else {
            if (file_put_contents(base_path('public/js/usuarios.json'), stripslashes(Usuario::editar($request)))) {
                \Session::flash('mensagemPop', ['msg' => 'Usuário Editado', 'class' => 'success']);
            } else {
                \Session::flash('mensagemPop', ['msg' => 'Houve um problema ao editar o usuario, por favor tente novamente', 'class' => 'error']);
            }
        }
        return redirect()->route('home');
    }

    function getById($id)
    {
        return response()->json(Usuario::findById($id));
    }

}
