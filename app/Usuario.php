<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{

    private static $id;
    private static $email;
    private static $tel;
    private static $usuario;
    private static $hasError = false;

    public static $rules = [
        'nome' => 'required|min:8',
        'email' => 'required|email',
        'tel' => 'required|min:15',
    ];

    public static $mensagens = [
        'nome.required' => 'Informe o nome do usuário',
        'nome.min' => 'Informe o nome do usuário com no minimo 8 caracteres',
        'email.required' => 'Informe o email do usuário',
        'email.email' => 'Informe um email válido',
        'tel.required' => 'Informe o telefone do usuário',
        'tel.min' => 'Informe um telefone válido'
    ];

    public static function salvar($request)
    {
        $jsonString = file_get_contents(asset('js/usuarios.json'));
        $usuarios = json_decode($jsonString, true);
        $lastUserId = end($usuarios)['id'];
        $newUser = array("id" => $lastUserId + 1, "nome" => $request->nome, "email" => $request->email, "telefone" => $request->tel);
        $usuarios[] = $newUser;

        return json_encode($usuarios, JSON_UNESCAPED_UNICODE);
    }

    public static function apagar($request)
    {

        Usuario::$id = $request->deleteId;
//      lê o json file, decodifica e filtra os itens
        $jsonString = file_get_contents(asset('js/usuarios.json'));
        $usuarios = json_decode($jsonString, true);
        $novoArray = array_filter($usuarios, function ($value) {
            return $value['id'] != Usuario::$id;
        });

        return json_encode($novoArray, JSON_UNESCAPED_UNICODE);
    }

    public static function editar($request)
    {
        Usuario::$id = $request->idEdit;
        $usuario = array("id" => $request->idEdit, "nome" => $request->nome, "email" => $request->email, "telefone" => $request->tel);

//      lê o json file, decodifica e filtra os itens
        $jsonString = file_get_contents(asset('js/usuarios.json'));
        $usuarios = json_decode($jsonString, true);
        $novoArray = array_filter($usuarios, function ($value) {
            return $value['id'] != Usuario::$id;
        });
        $novoArray[] = $usuario;

        return json_encode($novoArray, JSON_UNESCAPED_UNICODE);
    }

    public static function findById($id)
    {
        Usuario::$id = $id;
//      lê o json file, decodifica e filtra o item
        $jsonString = file_get_contents(asset('js/usuarios.json'));
        $usuarios = json_decode($jsonString, true);
        array_filter($usuarios, function ($value) {
            if ($value['id'] == Usuario::$id) {
                Usuario::$usuario = $value;
            }
        });
        return Usuario::$usuario;
    }

    public static function verificaEmailAndTel($email, $tel, $id = null)
    {
        Usuario::$email = $email;
        Usuario::$tel = $tel;
        if (isset($id)) {
            Usuario::$id = $id;
        }
        $jsonString = file_get_contents(asset('js/usuarios.json'));
        $usuarios = json_decode($jsonString, true);
        array_filter($usuarios, function ($value) {
            if (isset(Usuario::$id)) {
                if ($value['email'] == Usuario::$email || $value['telefone'] == Usuario::$tel) {
                    if ($value['id'] != Usuario::$id) {
                        Usuario::$hasError = true;
                    }
                }
            } else {
                if ($value['email'] == Usuario::$email || $value['telefone'] == Usuario::$tel) {
                    Usuario::$hasError = true;
                }
            }
        });
        return Usuario::$hasError;
    }


}
